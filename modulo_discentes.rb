module Discentes
    class Aluno
        attr_accessor :nome, :matricula, :curso

    end
    class AlunoGraduacao < Aluno
        attr_accessor :cr, :carga_horaria
        def initialize nome, matricula, curso, cr, carga_horaria
            @nome = nome
            @matricula = matricula
            @curso = curso
            @cr = cr
            @carga_horaria = carga_horaria
        end
        def salvar
            {nome: @nome, matricula: @matricula, curso: @curso, cr: @cr, carga_horaria: @carga_horaria}
        end

    class AlunoMestrado < Aluno
        attr_accessor :graduacao, :orientador, :area_pesquisa
        def initialize nome, matricula, curso, grad, orientador, area_pesquisa
            @nome = nome
            @matricula = matricula
            @curso = curso
            @graduacao = grad
            @orientador = orientador
            @area_pesquisa = area_pesquisa
        def salvar
            {nome: @nome, matricula: @matricula, curso: @curso, graduacao: @graduacao, orientador: @orientador, area_pesquisa: @area_pesquisa}
        end
        end 
    end
end