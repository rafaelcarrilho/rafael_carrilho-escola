require_relative "modulo_bd"
require_relative "modulo_discentes"
require_relative "modulo_docentes"

include Docentes::Departamento
GMA = Departamento.new ["Calculos", "Algebra", "Probabilidade"], "UFASA", "GMA"
GFI = Departamento.new ["Fisicas", "Fisica Experimental"], "Bloco de Fisica", "GFI"
TCC = Departamento.new ["Progs", "Estrutura de Dados", "Banco de Dados"], "IC", "TCC"

include Docentes::Professor
rossetti = Professor.new 7500.52, "Rossetti", "123456789", 60, TCC
begoña = Professor.new 21000.71, "Begoña", "987654321", 45, GMA
irineu = Professor.new 871253.52, "Irineu", "33333333333", 102, GFI

include Discente::AlunoGraduacao
rafael = AlunoGraduacao.new "Rafael", "18200", "CC", 7.5 , 50%

include Discente::AlunoMestrado
ricardo = AlunoMestrado.new "Ricardo", "18201", "Engenharia", "Engenharia", "Irineu", "Fisica Nuclear"

include Bd::Bd
tabela_prof = Bd.new
tabela_prof.salvar rossetti
tabela_prof.salvar begoña
tabela_prof.salvar irineu

tabela_aluno_grad = Bd.new
tabela_aluno_grad.salvar rafael

tabela_aluno_mest = Bd.new
tabela_aluno_mest.salvar ricardo
