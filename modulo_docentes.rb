module Docentes
    class Professor
        attr_accessor :salario, :nome, :matricula, :idade, :departamento
        def initialize sal, nome, mat, id, dep
            @salario = sal
            @nome = nome
            @matricula = mat
            @idade = id
            @departamento = dep
        end
        def salvar
            {nome: @nome, salario: @salario, matricula: @matricula, idade: @idade, departamento: @departamento}
        end
    end
    class Departamento
        attr_accessor :materias, :predio, :sigla
        def initialize materias,, predio, sigla
            @materias = materias
            @predio = predio
            @sigla = sigla
        end
    end
end 
